'''
This program allows OpenStreetBugs to be imported into the new Notes
feature of OpenStreetMap. Please use it (and any derived works) only
to import OpenStreetBugs to Notes in your local environment, not on
a large scale.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import argparse
import re
import urllib
import urllib2
import json
import sys
import time

def valid_bbox(bbox_string):
  match = re.match('^([0-9]+(\\.[0-9]+)?),([0-9]+(\\.[0-9]+)?),([0-9]+(\\.[0-9]+)?),([0-9]+(\\.[0-9]+)?)$', bbox_string)
  if not match:
    raise argparse.ArgumentTypeError("Invalid Bounding Box")

  left   = float(match.group(1))
  bottom = float(match.group(3))
  right  = float(match.group(5))
  top    = float(match.group(7))

  if top > 90. or top < -90. :
    raise argparse.ArgumentTypeError("This is invalid: top is outside the range [-90,90].")
  
  if left > 180. or left < -180. :
    raise argparse.ArgumentTypeError("This is invalid: left is outside the range [-180,180].")
  
  if bottom > 90. or bottom < -90. :
    raise argparse.ArgumentTypeError("This is invalid: bottom is outside the range [-90,90].")
  
  if right > 180. or right < -180. :
    raise argparse.ArgumentTypeError("This is invalid: right is outside the range [-180,180].")
  
  if top <= bottom :
    raise argparse.ArgumentTypeError("This is invalid: top must be larger than bottom.")

  if right <= left :
    raise argparse.ArgumentTypeError("This is invalid: right must be larger than left.")

  return [left, bottom, right, top]


def create_osm_note (bug) :
  (mid, lon, lat, desc, status) = bug

  content = {
    "lat": lat,
    "lon": lon,
    "text": "Transfered from OpenStreetBugs\n%s" % re.sub("<hr />", "\n", desc)
  }
  post_url = "http://api.openstreetmap.org/api/0.6/notes?%s" % urllib.urlencode(content)
  request = urllib2.Request(post_url, "")
  response = urllib2.urlopen(request)
  response.read()



def comment_and_close_osb (bug) :
  (mid, lon, lat, desc, status) = bug

  content = {
    "id": mid,
    "text": "Transfered to OSM Notes"
  }
  post_url = "http://openstreetbugs.schokokeks.org/api/0.1/editPOIexec?%s" % urllib.urlencode(content)
  request = urllib2.Request(post_url, "")
  response = urllib2.urlopen(request)
  response.read()
  
  close_osb(bug)

def close_osb (bug) :
  (mid, lon, lat, desc, status) = bug

  content = {
    "id": mid
  }
  post_url = "http://openstreetbugs.schokokeks.org/api/0.1/closePOIexec?%s" % urllib.urlencode(content)
  request = urllib2.Request(post_url, "")
  response = urllib2.urlopen(request)
  response.read()




parser = argparse.ArgumentParser(description='OpenStreetBugs to Notes transfer')
parser.add_argument("bbox", type=valid_bbox, 
                    help="specify bounding box (as left,bottom,right,top)")
parser.add_argument("--filter", type=str,
                    help="specify string to filter for")

args = vars(parser.parse_args())
bbox = args["bbox"]
filter_text = args["filter"]

get_bugs_url = 'http://openstreetbugs.schokokeks.org/api/0.1/getBugs?b=%s&t=%s&l=%s&r=%s' % (bbox[1], bbox[3], bbox[0], bbox[2])
bugs_as_javascript = urllib2.urlopen(get_bugs_url).read()


bugs = []
pattern = re.compile("putAJAXMarker\\(([0-9]+), *([0-9]+\\.[0-9]+), *([0-9]+\\.[0-9]+), *'([^']*)', *([0-9]+)\\)", re.U)
for match in pattern.finditer(bugs_as_javascript):
  marker = match.groups()
  (mid, lon, lat, desc, status) = marker
  if status == "0" :
    bugs.append(marker)


if len(bugs) == 0 :
  print "There are no OpenStreetBugs in this bbox."  
  sys.exit(1)


for bug in bugs :
  (mid, lon, lat, desc, status) = bug

  if filter_text and not re.search(filter_text, desc) : 
    continue

  print "\n OpenStreetBug #%s   @   %s, %s :\n%s" % (mid, lon, lat, re.sub("<hr />", "\n", desc))

  while True :
    ch = raw_input("\nTransfer to OSM Notes (yes/no/close/help)? ").lower()

    if ch == 'y' :
      print "Uploading to OSM Notes and closing the original bug..."
      create_osm_note(bug)
      comment_and_close_osb(bug)
      print "done."
      break
    if ch == 'n' :
      print "This OpenStreetBug is left untouched."
      break
    if ch == 'c' :
      print "Closing this bug..."
      close_osb(bug)
      break
    if ch == 'h' :
      print "Say (y)es to transfer this bug from OpenStreetBugs to OSM Notes,"
      print "    (n)o to leave it in OpenStreetBugs,"
      print "    (c)lose to close the OpenStreetBugs entry, but not upload it to OSM notes,"
      print "    (h)elp to display this help.\n"
    else :
      print "Try one of the letters y/n/c/h. "

  time.sleep(1)

sys.exit(0)